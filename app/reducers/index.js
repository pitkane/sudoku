import sudoku from './sudoku'
import testing from './testing'

export {
  sudoku,
  testing,
}
